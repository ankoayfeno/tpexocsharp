using System;
using System.Collections.Generic;

namespace TpExoCsharp
{
    public class Player
    {
        public string Name { get; }
        public int Score { get; set; }

        public Player(string name)
        {
            Name = name;
            Score = 0;
        }
    }

    public class DiceGame
    {
        private List<Player> players;
        private Random random;

        public DiceGame()
        {
            players = new List<Player>();
            random = new Random();
        }

        public void AddPlayer(string name)
        {
            Player newPlayer = new Player(name);
            players.Add(newPlayer);
        }

        public void ShowScore()
        {
            Console.WriteLine("Score actuel :");
            players.Sort((x, y) => y.Score.CompareTo(x.Score));
            foreach (var player in players)
            {
                Console.WriteLine($"- {player.Name} : {player.Score} points");
            }
        }

        public void Play()
        {
            Console.WriteLine("Bienvenue au Jeu de Dé !");
            Console.WriteLine("Combien de joueurs participent ?");
            int numPlayers = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < numPlayers; i++)
            {
                Console.WriteLine($"Nom du joueur {i + 1}: ");
                string playerName = Console.ReadLine();
                AddPlayer(playerName);
            }

            while (true)
            {
                Console.Clear();
                ShowScore();

                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Nouvelle manche");
                Console.WriteLine("2 - Voir l'historique des manches");
                Console.WriteLine("3 - Quitter");
                Console.Write("Quel est votre choix ? ");

                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        NewRound();
                        break;
                    case "2":
                        ShowHistory();
                        break;
                    case "3":
                        Console.WriteLine("Au revoir !");
                        return;
                    default:
                        Console.WriteLine("Option invalide. Veuillez réessayer.");
                        break;
                }
            }
        }

        private void NewRound()
        {
            Console.Clear();
            foreach (var player in players)
            {
                int roll = random.Next(1, 7);
                Console.WriteLine($"Tour de {player.Name} - Voulez-vous lancer le dé ? (y or n)");
                Console.WriteLine($"Le dé est lancé... Résultat : {roll}");

                if (roll == 6)
                {
                    player.Score += 2;
                }

                Console.ReadKey();
                Console.Clear();
            }

            int maxScore = FindMaxScore();
            foreach (var player in players)
            {
                if (player.Score == maxScore)
                {
                    player.Score += 1;
                }
            }
        }

        private int FindMaxScore()
        {
            int maxScore = int.MinValue;
            foreach (var player in players)
            {
                if (player.Score > maxScore)
                {
                    maxScore = player.Score;
                }
            }
            return maxScore;
        }

        private void ShowHistory()
        {
            
            Console.WriteLine("Historique des manches:");
            Console.WriteLine("Pas d'historique disponible pour le moment.");

            Console.WriteLine("Appuyez sur une touche pour revenir au menu principal.");
            Console.ReadKey();
        }
    }

    public class JeuxDeDes
    {
        public static void Jouer()
        {
            DiceGame game = new DiceGame();
            game.Play();
        }
    }
}
