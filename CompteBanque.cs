using System;

namespace TpExoCsharp
{
    public class Client
    {
        public string CIN { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Tel { get; set; }

        public Client(string cin, string nom, string prenom, string tel)
        {
            CIN = cin;
            Nom = nom;
            Prenom = prenom;
            Tel = tel;
        }

        public Client(string cin, string nom, string prenom) : this(cin, nom, prenom, "")
        {
        }

        public void Afficher()
        {
            Console.WriteLine("Informations du client :");
            Console.WriteLine($"CIN: {CIN}");
            Console.WriteLine($"Nom: {Nom}");
            Console.WriteLine($"Prénom: {Prenom}");
            Console.WriteLine($"Tél: {Tel}");
        }
    }

    public class Compte
    {
        private static int nbComptes = 0;

        public int NumeroCompte { get; }
        public double Solde { get; private set; }
        public Client Proprietaire { get; }

        public Compte(Client proprietaire)
        {
            nbComptes++;
            NumeroCompte = nbComptes;
            Solde = 0;
            Proprietaire = proprietaire;
        }

        public void Crediter(double montant)
        {
            Solde += montant;
            Console.WriteLine("Opération bien effectuée");
        }

        public void Crediter(double montant, Compte compte)
        {
            Solde += montant;
            compte.Debiter(montant);
            Console.WriteLine("Opération bien effectuée");
        }

        public void Debiter(double montant)
        {
            if (Solde >= montant)
            {
                Solde -= montant;
                Console.WriteLine("Opération bien effectuée");
            }
            else
            {
                Console.WriteLine("Solde insuffisant");
            }
        }

        public void Debiter(double montant, Compte compte)
        {
            if (Solde >= montant)
            {
                Solde -= montant;
                compte.Crediter(montant);
                Console.WriteLine("Opération bien effectuée");
            }
            else
            {
                Console.WriteLine("Solde insuffisant");
            }
        }

        public void AfficherResume()
        {
            Console.WriteLine("************************");
            Console.WriteLine($"Numéro de Compte: {NumeroCompte}");
            Console.WriteLine($"Solde de compte: {Solde}");
            Console.WriteLine("Propriétaire du compte :");
            Proprietaire.Afficher();
            Console.WriteLine("************************");
        }

        public static int GetNombreComptesCrees()
        {
            return nbComptes;
        }
    }

    public class CompteBanque
    {
        public static void Compte()
        {
            Console.WriteLine("Compte 1:");
            Console.WriteLine("Donner Le CIN: ");
            string cin = Console.ReadLine();

            Console.WriteLine("Donner Le Nom: ");
            string nom = Console.ReadLine();

            Console.WriteLine("Donner Le Prénom: ");
            string prenom = Console.ReadLine();

            Console.WriteLine("Donner Le numéro de télephone: ");
            string tel = Console.ReadLine();

            Client proprietaire = new Client(cin, nom, prenom, tel);
            Compte compte1 = new Compte(proprietaire);

            Console.WriteLine("Détails du compte:");
            compte1.AfficherResume();

            Console.WriteLine("Donner le montant à déposer:");
            double montantDepot = Convert.ToDouble(Console.ReadLine());
            compte1.Crediter(montantDepot);
            compte1.AfficherResume();

            Console.WriteLine("Donner le montant à retirer:");
            double montantRetrait = Convert.ToDouble(Console.ReadLine());
            compte1.Debiter(montantRetrait);
            compte1.AfficherResume();
        }
    }
}
