using System;
using System.Collections.Generic;

namespace TpExoCsharp
{
    public partial class Alarm
    {
        public string Name { get; set; }
        public string Time { get; set; }
        public string Date { get; set; }
        public bool SingleTime { get; set; }
        public bool IsPeriodic { get; set; }
        public bool DefaultRingtone { get; set; }

        public Alarm(string name, string time, string date, bool singleTime, bool isPeriodic, bool defaultRingtone)
        {
            Name = name;
            Time = time;
            Date = date;
            SingleTime = singleTime;
            IsPeriodic = isPeriodic;
            DefaultRingtone = defaultRingtone;
        }
    }

    public partial class Clock
    {
        public string Time { get; set; }
        public string Location { get; set; }
        public string Date { get; set; }
        public int TimeDifference { get; set; }

        public Clock(string time, string location, string date, int timeDifference)
        {
            Time = time;
            Location = location;
            Date = date;
            TimeDifference = timeDifference;
        }
    }

    public partial class HorlogeApp
    {
        private List<Alarm> alarms;
        private List<Clock> clocks;

        public HorlogeApp()
        {
            alarms = new List<Alarm>();
            clocks = new List<Clock>();
        }

        public void MainMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Bienvenue dans le menu principal ( * v * )");
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Alarme");
                Console.WriteLine("2 - Horloge");
                Console.WriteLine("3 - Quitter");
                Console.Write("Quel est votre choix ? ");

                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        AlarmMenu();
                        break;
                    case "2":
                        ClockMenu();
                        break;
                    case "3":
                        Console.WriteLine("Au revoir !");
                        return;
                    default:
                        Console.WriteLine("Option invalide. Veuillez réessayer.");
                        break;
                }
            }
        }

        public void AlarmMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Voir les Alarmes actives");
                Console.WriteLine("2 - Créer une alarme");
                Console.WriteLine("3 - Retour au menu principal");
                Console.Write("Quel est votre choix ? ");

                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        ShowActiveAlarms();
                        break;
                    case "2":
                        CreateAlarm();
                        break;
                    case "3":
                        return;
                    default:
                        Console.WriteLine("Option invalide. Veuillez réessayer.");
                        break;
                }
            }
        }

        public void ShowActiveAlarms()
        {
            Console.WriteLine("Liste d'alarmes actives :");
            foreach (var alarm in alarms)
            {
                Console.WriteLine($"{alarm.Name} - {alarm.Time}");
                if (alarm.SingleTime)
                    Console.WriteLine(alarm.Date);
                if (alarm.IsPeriodic)
                    Console.WriteLine("Périodique");
                Console.WriteLine();
            }
            Console.WriteLine("Appuyez sur une touche pour revenir au menu.");
            Console.ReadKey();
        }

        public void CreateAlarm()
        {
            Console.WriteLine("Info de la nouvelle Alarme :");
            Console.Write("Donnez un nom de référence : ");
            string name = Console.ReadLine();

            Console.Write("Heure de l'alarme (hh:mm) : ");
            string time = Console.ReadLine();

            Console.Write("Date de planification (L/M/ME/J/V/S/D) : ");
            string date = Console.ReadLine();

            Console.Write("Lancer une seule fois (y/n) : ");
            bool singleTime = Console.ReadLine().ToLower() == "y" ? true : false;

            Console.Write("Périodique (y/n) : ");
            bool isPeriodic = Console.ReadLine().ToLower() == "y" ? true : false;

            Console.Write("Activer sonnerie par defaut (y/n) : ");
            bool defaultRingtone = Console.ReadLine().ToLower() == "y" ? true : false;

            Alarm newAlarm = new Alarm(name, time, date, singleTime, isPeriodic, defaultRingtone);
            alarms.Add(newAlarm);

            Console.WriteLine("Votre nouvelle alarme est enregistrée :)");
            Console.WriteLine("Appuyez sur une touche pour revenir au menu.");
            Console.ReadKey();
        }

        public void ClockMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Voir les Horloges actives");
                Console.WriteLine("2 - Ajouter une horloge");
                Console.WriteLine("3 - Retour au menu principal");
                Console.Write("Quel est votre choix ? ");

                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        ShowActiveClocks();
                        break;
                    case "2":
                        AddClock();
                        break;
                    case "3":
                        return;
                    default:
                        Console.WriteLine("Option invalide. Veuillez réessayer.");
                        break;
                }
            }
        }

        public void ShowActiveClocks()
        {
            Console.WriteLine("Liste d'horloges :");
            foreach (var clock in clocks)
            {
                Console.WriteLine($"{clock.Location} - {clock.Time}");
                Console.WriteLine($"{clock.TimeDifference}h");
                Console.WriteLine();
            }
            Console.WriteLine("Appuyez sur une touche pour revenir au menu.");
            Console.ReadKey();
        }

        public void AddClock()
        {
            Console.Write("Choisissez une ville [Moscou, Dubaï, Mexique] : ");
            string city = Console.ReadLine();

            
            string currentTime = DateTime.Now.ToString("HH:mm:ss");
            string currentDate = DateTime.Now.ToString("ddd dd MMM");
            int timeDifference = 0;

            if (city == "Moscou")
                timeDifference = 3;
            else if (city == "Dubaï")
                timeDifference = 2;
            else if (city == "Mexique")
                timeDifference = -7;

            Clock newClock = new Clock(currentTime, city, currentDate, timeDifference);
            clocks.Add(newClock);

            Console.WriteLine("Votre nouvelle horloge est enregistrée !");
            Console.WriteLine("Appuyez sur une touche pour revenir au menu.");
            Console.ReadKey();
        }
    }

    public partial class Horloge
    {
        public static void HorlogeApp()
        {
            HorlogeApp app = new HorlogeApp();
            app.MainMenu();
        }
    }
}
